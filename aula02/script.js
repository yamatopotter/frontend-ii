// Unificação das funções verificaNome e verificaTelefone

function verificaConteudo(texto){
    let conteudo = prompt(`Qual seu ${texto}?`);
    if (conteudo !== null){
        return conteudo;
    }
    else{
        alert(`É preciso informar um ${texto}`);
        return null;
    }
}

// Funções criadas separadas para cada tarefa

function verificaNome(){
    let nome = prompt("Qual seu nome completo?");
    if (nome !== null){
        return nome;
    }
    else{
        alert("É preciso informar um nome");
        return null;
    }
}

function verificaTelefone(){
    let telefone = prompt("Qual seu telefone");
    console.log(telefone);

    if (telefone !== null){
        return telefone;
    }
    else{
        alert("É preciso informar um telefone");
        return null
    }
}

// Fim das funções criadas para cada tarefa

var nome = null;

while (nome === null){
    nome = verificaConteudo("nome completo");
}
console.log(`Nome: ${nome}`);

var telefone = null;

while (telefone === null){
    telefone = verificaConteudo("telefone");
}
console.log(`Telefone: ${telefone}`);

var isWhatsapp = confirm("Esse número é Whatsapp?");
console.log(`Aceita receber mensagem por WhatsApp:${isWhatsapp}.`);

alert(`Muito obrigado ${nome}, um de nossos consultores entrará em contato contigo através do número ${telefone}.`);