let listaItems = document.querySelectorAll('.lista__item');

let textoItem = listaItems[0].innerText;
let item = listaItems[0];

item.classList.add('removido');
item.innerText=`${textoItem} - Removido`;

textoItem = listaItems[1].innerText;
item = listaItems[1];

item.classList.add('selecionado');
item.innerText=`${textoItem} - Selecionado`;

textoItem = listaItems[2].innerText;
item = listaItems[2];

item.classList.add('selecionado');
item.innerText=`${textoItem} - Selecionado`;

textoItem = listaItems[3].innerText;
item = listaItems[3];

item.classList.add('removido');
item.innerText=`${textoItem} - Removido`;

textoItem = listaItems[4].innerText;
item = listaItems[4];

item.classList.add('removido');
item.innerText=`${textoItem} - Removido`;

function changeStatus(element){

    if(element.classList.contains('removido')){
        element.classList.remove('removido');
        element.classList.add('selecionado');

        let elementText = element.innerText;
        let splitText = elementText.split(" - ");

        element.innerText = `${splitText[0]} - Selecionado`;
    }
    else{
        element.classList.remove('selecionado');
        element.classList.add('removido');

        let elementText = element.innerText;
        let splitText = elementText.split(" - ");

        element.innerText = `${splitText[0]} - Removido`;
    }

}

