var sectionCarrinho = document.getElementById('carrinho');

var listaCarrinho = document.createElement('ul');

listaCarrinho.classList.add('lista');

sectionCarrinho.appendChild(listaCarrinho);

conteudoListaCarrinho = ['Carro', 'Bike', 'Patinete', 'Celular', 'Carregador'];

var itemListaCarrinho = [];

conteudoListaCarrinho.forEach( (conteudo)=>{
    let auxElement = document.createElement('li');
    auxElement.classList.add('lista__item');

    let auxText = document.createTextNode(conteudo);

    auxElement.appendChild(auxText);

    itemListaCarrinho.push(auxElement);
});

itemListaCarrinho.forEach((item)=>{
    listaCarrinho.appendChild(item);
});

// Primeira versão com FOR
// for (let i=0; i<conteudoListaCarrinho.length; i++){
//     let auxElement = document.createElement('li');
//     auxElement.classList.add('lista__item');

//     let auxText = document.createTextNode(conteudoListaCarrinho[i]);

//     auxElement.appendChild(auxText);

//     itemListaCarrinho.push(auxElement);
// }

// Primeira versão com FOR
// for (let i=0; i<itemListaCarrinho.length; i++){
//     listaCarrinho.appendChild(itemListaCarrinho[i])
// }

console.log(document.getElementsByTagName('body'));
