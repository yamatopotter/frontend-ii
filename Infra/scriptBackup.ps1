$sourcePath = "/home/matheus/teste/*"
$destinyPath ="$(pwd)/backup/"

$date = Get-Date -Format d-M-yyy

Copy-Item $sourcePath $destinyPath -Recurse -Verbose *> "$destinyPath\log-$date.txt"
