#encoding:UTF-8
import random

while True: 
    aleatorio = random.randrange(5)
    escolhaPc = ""
    print("1)Pedra")
    print("2)Papel")
    print("3)Tesoura")
    print("4)Spock")
    print("5)Lagarto")
    print("6)Sair do Programa")
    opcao = int(input("O que você escolhe: "))
    
    if opcao == 1:
        escolhaUsuario = "pedra"
    elif opcao == 2:
        escolhaUsuario = "papel"
    elif opcao == 3:
        escolhaUsuario = "tesoura"
    elif opcao == 4:
        escolhaUsuario = "spock"
    elif opcao == 5:
        escolhaUsuario = "lagarto"
    elif opcao == 6:
        print ("Nos vemos!")
        break
    else:
        print ("Valor Invalido")
        continue
        
    print("Tua escolha: ", escolhaUsuario)   
    if aleatorio == 0:
        escolhaPc = "pedra"
    elif aleatorio == 1:
        escolhaPc = "papel"
    elif aleatorio == 2:
        escolhaPc = "tesoura"
    elif aleatorio == 3:
        escolhaPc = "spock"
    elif aleatorio == 4:
        escolhaPc = "lagarto"
    print("PC escolheu: ", escolhaPc)
    print("...")
    
    # -----Jogadas da pedra-----

    # pedra x papel
    if escolhaPc == "pedra" and escolhaUsuario == "papel":
        print("Ganhou, papel cobre pedra")
    if escolhaUsuario == "pedra" and escolhaPc == "papel":
        print("Perdeu, papel cobre pedra")

    # pedra x lagarto
    if escolhaPc == "pedra" and escolhaUsuario == "lagarto":
        print("Perdeu, pedra esmaga lagarto")
    if escolhaUsuario == "pedra" and escolhaPc == "lagarto":
        print("Ganhou, pedra esmaga lagarto")

    #pedra x tesoura

    if escolhaPc == "pedra" and escolhaUsuario == "tesoura":
        print("Perdeu, pedra esmaga tesoura")
    if escolhaUsuario == "pedra" and escolhaPc == "tesoura":
        print("Ganhou, pedra esmaga tesoura")

    #pedra x spock
    if escolhaPc == "pedra" and escolhaUsuario == "spock":
        print("Ganhou, Spock vaporiza pedra")
    if escolhaUsuario == "pedra" and escolhaPc == "spock":
        print("Perdeu, Spock vaporiza pedra")

    #-----Jogadas de Papel-----

    #papel x tesoura
    if escolhaPc == "papel" and escolhaUsuario == "tesoura":
        print("Ganhou, tesoura corta papel")
    if escolhaUsuario == "papel" and escolhaPc == "tesoura":
        print("Perdeu, tesoura corta papel")

    #papel x lagarto
    if escolhaPc == "papel" and escolhaUsuario == "lagarto":
        print("Ganhou, lagarto come papel")
    if escolhaUsuario == "papel" and escolhaPc == "lagarto":
        print("Perdeu, lagarto come papel")

    #papel x spock
    if escolhaPc == "papel" and escolhaUsuario == "spock":
        print("Ganhou, papel refuta Spock")
    if escolhaUsuario == "papel" and escolhaPc == "spock":
        print("Perdeu, papel refuta Spock")
    
    # ------Jogadas da Tesoura -----

    #tesoura x spock

    if escolhaPc == "tesoura" and escolhaUsuario == "spock":
        print("Ganhou, Spock quebra tesoura")
    if escolhaUsuario == "tesoura" and escolhaPc == "spock":
        print("Perdeu, Spock quebra tesoura")

    #tesoura x lagarto

    if escolhaPc == "tesoura" and escolhaUsuario == "lagarto":
        print("Perdeu, tesoura decapta lagarto")
    if escolhaUsuario == "tesoura" and escolhaPc == "lagarto":
        print("Ganhou, tesoura decapta lagarto")

    #-----Jogada Spock-----

    #spock x lagarto
    if escolhaPc == "spock" and escolhaUsuario == "lagarto":
        print("Ganhou, lagarto envenena Spock")
    if escolhaUsuario == "spock" and escolhaPc == "lagarto":
        print("Perdeu, lagarto envenena Spock")
        
    #Empate
    if escolhaPc == escolhaUsuario:
        print("Empate")

    again = input("Jogar novamente? s/n: ")
    if 's' in again:
        continue
    elif 'n' in again:
        print("Nos vemos!")
        break
    else:
        print("Valor Invalido")